package com.BasicClothes.restful_web_api;

import com.BasicClothes.restful_web_api.dto.userDTOs.RegisterUserDTO;
import com.BasicClothes.restful_web_api.helper.UserHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableFeignClients
@EnableScheduling
public class AuthenticationServiceApplication {

    @Autowired
    UserHelper userHelper;

    public static void main(String[] args) {
        SpringApplication.run(AuthenticationServiceApplication.class, args);
    }

}
