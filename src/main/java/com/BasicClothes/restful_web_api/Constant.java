package com.BasicClothes.restful_web_api;

public class Constant {
    public static class Security {
        public static final String[] PUBLIC_ENDPOINTS = new String[]{
                "/user/authenticate",
                "/user/refreshExpiredCredentials",
                "/user/resetPassword",
                "/user/register"
        };

        public static final String PROTECTED = "PASSWORD_PROTECTED";

        public static final String RESET_CREDENTIALS_MESSAGE = "Credentials resettled.\n" +
                "Username: %s\n" +
                "Password: %s";

        public static final String ACCOUNT_CREATION_MESSAGE = "Thanks to signed up to the service.\n" +
                "To access to the platform use the following credentials:\n\n" +
                "Username: %s\n" +
                "Password: The one settled during registration process.\n\n" +
                "Enjoy the service";
        public static final String ORDER_DONE_MITTENTE = "NEW ORDER!!\n" +
                "Article: %s\n" +
                "Username: %s\n" +
                "City: %s\n" +
                "Indirizzo: %s\n" +
                "Cap: %s\n" +
                "Enjoy!!";
        public static final String ORDER_DONE_DESTINATARIO = "NEW ORDER!!\n" +
                "Article: %s\n" +
                "Sender email: %s\n"+
                "Username: %s\n\n" +
                "Your order will arrive in 5 days\n"+
                "Enjoy!!";
        public static final String RESO_DONE_MITTENTE = "NEW RETURNED!!\n" +
                "Article: %s\n" +
                "Sender email: %s\n"+
                "Username: %s\n\n" +
                "Enjoy!!";
        public static final String RESO_DONE_DESTINATARIO = "NEW RETURNED!!\n" +
                "Article: %s\n" +
                "Username: %s\n" +
                "City: %s\n" +
                "Indirizzo: %s\n" +
                "Cap: %s\n" +
                "Enjoy!!";
    }
}
