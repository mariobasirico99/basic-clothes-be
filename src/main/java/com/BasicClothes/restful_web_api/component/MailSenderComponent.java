package com.BasicClothes.restful_web_api.component;

import com.BasicClothes.restful_web_api.dto.mailDTOs.MailMessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class MailSenderComponent {

    @Autowired
    JavaMailSender sender;

    @Value("${spring.mail.username}")
    private String username;

    public void sendMessage(MailMessageDTO mailMessageDTO) {
        sender.send(generateMessage(mailMessageDTO));
    }

    private SimpleMailMessage generateMessage(MailMessageDTO mailMessageDTO) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(username);
        message.setTo(mailMessageDTO.receiver);
        message.setSubject(mailMessageDTO.subject);
        message.setText(mailMessageDTO.text);
        return message;
    }
}
