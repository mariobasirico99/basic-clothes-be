package com.BasicClothes.restful_web_api.controller;

import com.BasicClothes.restful_web_api.annotation.security.AllowAdmin;
import com.BasicClothes.restful_web_api.annotation.security.AllowAnyAuthenticated;

import com.BasicClothes.restful_web_api.dto.articleDTOs.AddArticleDTO;


import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.BasicClothes.restful_web_api.model.Article;
import com.BasicClothes.restful_web_api.service.ArticleServiceImpl;

import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    ArticleServiceImpl articleService;

    @GetMapping("/getall")
    @AllowAnyAuthenticated
    public List<Article> getAll() {
        return articleService.findAllArticle();
    }

    @GetMapping("/notMineAndSex")
    @AllowAnyAuthenticated
    public List<Article> findArticleNotMine(@RequestParam("id") String id,@RequestParam("sex") String sesso) {
        return articleService.findArticlesNotMineAndSex(Long.valueOf( id ),sesso);
    }

    @GetMapping("/notMine")
    @AllowAnyAuthenticated
    public List<Article> findArticleNotMine(@RequestParam("id") String id) {
        return articleService.findArticlesNotMine(Long.valueOf( id ));
    }

    @GetMapping("/getUserIdById")
    @AllowAnyAuthenticated
    public Long getUserById(@RequestParam("id") String id) {
        return articleService.findUserById(Long.valueOf( id ));
    }

    @PostMapping("/add")
    public Article add(@RequestBody AddArticleDTO addArticleDTO) {
        return articleService.add(addArticleDTO);
    }

    @PatchMapping("/upload")
    public BodyBuilder uploadImage(@RequestParam("imageValue") MultipartFile file, @RequestParam("id") String id) throws IOException {
        byte[] result = articleService.compressBytes(file.getBytes());
        articleService.updateImage(file.getBytes(),Long.valueOf(id));
        return ResponseEntity.status(HttpStatus.OK);
    }
    @DeleteMapping("/delete")
    @Transactional
    public ResponseEntity delete(@RequestParam("id") String id) throws IOException {
        articleService.delete(Long.valueOf( id ));
        return ResponseEntity.ok("Done");
    }

    @PostMapping("/filter")
    @AllowAnyAuthenticated
    public List<Article> filter (@RequestBody AddArticleDTO example){
        return articleService.findByFilter(example);
    }

    @GetMapping("/getimage")
    @AllowAnyAuthenticated
    public byte[] getFile(@RequestParam("id") String id) throws IOException {
        byte[] img = articleService.getimage(Long.valueOf( id ));
        return img;
    }

    @GetMapping("/getById")
    @AllowAnyAuthenticated
    public Article getById(@RequestParam("id") String id) throws IOException{
        Article a = articleService.getById(Long.valueOf( id ));
        return a;
    }
}
