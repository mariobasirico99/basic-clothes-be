package com.BasicClothes.restful_web_api.controller;

import com.BasicClothes.restful_web_api.annotation.security.AllowAdmin;
import com.BasicClothes.restful_web_api.annotation.security.AllowAnyAuthenticated;
import com.BasicClothes.restful_web_api.dto.feedbackDTOs.AddFeedbackDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.AddOrderDTO;
import com.BasicClothes.restful_web_api.model.Feedback;
import com.BasicClothes.restful_web_api.model.Order;
import com.BasicClothes.restful_web_api.service.FeedbackServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/feedback")
public class FeedbackController {
    @Autowired
    FeedbackServiceImpl feedbackService;

    @GetMapping("/getall")
    @AllowAnyAuthenticated
    public List<Feedback> getAll() {
        return feedbackService.findAll();
    }

    @PostMapping("/add")
    @AllowAnyAuthenticated
    public Feedback add(@RequestBody AddFeedbackDTO addFeedbackDTO) {
        addFeedbackDTO.voto = Double.parseDouble(String.valueOf(addFeedbackDTO.voto));
        return feedbackService.add(addFeedbackDTO);
    }

    @GetMapping("/getByUser")
    @AllowAnyAuthenticated
    public @ResponseBody List<Feedback> getlist(@RequestParam("id") String id) throws IOException {
        List<Feedback> f = feedbackService.findFeedbackByUser_Id(Long.valueOf( id ));
        return f;
    }

    @GetMapping("/getRankingByUser")
    @AllowAnyAuthenticated
    public @ResponseBody Double getRanking(@RequestParam("id") String id) throws IOException {
        double ranking = feedbackService.getRanking(Long.valueOf( id ));
        return ranking;
    }
    @DeleteMapping("/delete")
    @AllowAnyAuthenticated
    public ResponseEntity delete(@RequestParam("id") String id) throws IOException {
        feedbackService.delete(Long.valueOf( id ));
        return ResponseEntity.ok("Done");
    }
}
