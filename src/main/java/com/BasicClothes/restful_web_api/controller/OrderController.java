package com.BasicClothes.restful_web_api.controller;

import com.BasicClothes.restful_web_api.annotation.security.AllowAdmin;
import com.BasicClothes.restful_web_api.annotation.security.AllowAnyAuthenticated;
import com.BasicClothes.restful_web_api.dto.articleDTOs.AddArticleDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.AddOrderDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.UpdateOrderDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.UpdateOrderPaymentDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.UpdateUserDTO;
import com.BasicClothes.restful_web_api.model.Article;
import com.BasicClothes.restful_web_api.model.Order;
import com.BasicClothes.restful_web_api.model.User;
import com.BasicClothes.restful_web_api.service.ArticleServiceImpl;
import com.BasicClothes.restful_web_api.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    OrderServiceImpl orderService;

    @GetMapping("/getall")
    @AllowAnyAuthenticated
    public List<Order> getAll() {
        return orderService.findAll();
    }

    @PostMapping("/add")
    @AllowAnyAuthenticated
    public Order add(@RequestBody AddOrderDTO addOrderDTO) {
        return orderService.add(addOrderDTO);
    }

    @PatchMapping("/update")
    @AllowAdmin
    public Order update(@RequestBody UpdateOrderDTO updateOrderDTO,@RequestParam String id) {
        return orderService.update(updateOrderDTO,Long.valueOf( id ));
    }
    @PatchMapping("/updatePayment")
    @AllowAdmin
    public Order updatePayment(@RequestBody UpdateOrderPaymentDTO updateOrderPaymentDTO,@RequestParam String id) {
        return orderService.updatePayment(updateOrderPaymentDTO,Long.valueOf( id ));
    }

    @PostMapping("/reso")
    @AllowAnyAuthenticated
    public Order reso(@RequestParam("id") String id) {
        Order order = orderService.getById(Long.valueOf( id ));
        return orderService.reso(order);
    }
    @GetMapping("/getById")
    @AllowAnyAuthenticated
    public Order getById(@RequestParam("id") String id) throws IOException {
        Order o = orderService.getById(Long.valueOf( id ));
        return o;
    }
    @GetMapping("/getByIdDest")
    @AllowAnyAuthenticated
    public List<Order> getByIdDest(@RequestParam("id") String id) {
        return orderService.getByIdDest(Long.valueOf( id ));
    }
    @GetMapping("/getByIdMitt")
    @AllowAnyAuthenticated
    public List<Order> getByIdMitt(@RequestParam("id") String id) {
        return orderService.getByIdMitt(Long.valueOf( id ));
    }
    @DeleteMapping("/delete")
    @AllowAnyAuthenticated
    @Transactional
    public ResponseEntity delete(@RequestParam("id") String id) throws IOException {
        orderService.delete(Long.valueOf( id ));
        return ResponseEntity.ok("Done");
    }

}
