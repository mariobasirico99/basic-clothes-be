package com.BasicClothes.restful_web_api.controller;

import com.BasicClothes.restful_web_api.annotation.security.AllowAdmin;
import com.BasicClothes.restful_web_api.annotation.security.AllowAnyAuthenticated;
import com.BasicClothes.restful_web_api.annotation.security.AllowUser;
import com.BasicClothes.restful_web_api.dto.AuthenticationResponseDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.*;
import com.BasicClothes.restful_web_api.model.User;
import com.BasicClothes.restful_web_api.service.UserServiceImpl;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserServiceImpl userService;

    @PostMapping("/register")
    public User register(@RequestBody RegisterUserDTO registerUserDTO) {
        return userService.register(registerUserDTO);
    }

    @PostMapping("/authenticate")
    public AuthenticationResponseDTO authenticate(@RequestBody UserAuthenticationDTO userAuthenticationDTO) {
        return userService.authenticate(userAuthenticationDTO);
    }
    @GetMapping("/getById")
    @AllowAnyAuthenticated
    public User getById(@RequestParam("id") String id) throws IOException {
        User user = userService.getById(Long.valueOf( id ));
        return user;
    }
    @Transactional
    @DeleteMapping("/delete")
    @AllowAdmin
    public ResponseEntity delete(@RequestParam("id") String id) throws IOException {
        userService.delete(Long.valueOf( id ));
        return ResponseEntity.ok("Done");
    }
    @PostMapping("/refreshExpiredCredentials")
    public User refreshExpiredCredentials(@RequestBody RefreshExpiredCredentialsDTO refreshExpiredCredentialsDTO) {
        return userService.refreshExpiredCredentials(refreshExpiredCredentialsDTO);
    }
    @PatchMapping("/update")
    @AllowAnyAuthenticated
    public User update(@RequestBody UpdateUserDTO updateUserDTO, @RequestParam String id) {
        return userService.update(updateUserDTO,Long.valueOf( id ));
    }

    @PostMapping("/changePassword")
    @AllowAnyAuthenticated
    public User changePassword(@RequestBody RefreshExpiredCredentialsDTO refreshExpiredCredentialsDTO, HttpServletRequest httpServletRequest) {
        return userService.changePassword(refreshExpiredCredentialsDTO, httpServletRequest);
    }

    @GetMapping("/getall")
    @AllowAdmin
    public List<User> getAll() {
        return userService.findAll();
    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest httpServletRequest) {
        userService.logout(httpServletRequest);
    }
}
