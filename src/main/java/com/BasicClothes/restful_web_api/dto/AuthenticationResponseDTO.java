package com.BasicClothes.restful_web_api.dto;

import com.BasicClothes.restful_web_api.model.Role;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.Instant;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class AuthenticationResponseDTO {
    public Long userId;
    public String username;
    public String token;
    public Instant issuedAt;
    public Instant expireAt;
    public List<Role> roles;
}
