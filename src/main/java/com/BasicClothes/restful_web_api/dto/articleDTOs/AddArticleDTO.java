
package com.BasicClothes.restful_web_api.dto.articleDTOs;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Base64;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddArticleDTO {
    public String nome;
    public Boolean venduto;
    public String marca;
    public String tipo;
    public String colore;
    public String taglia;
    public Double prezzo;
    public byte[] picture;
    public String file;
    public String sesso;
    public Long id_utente;
    public void setPicture(byte[] result) {
        this.picture = result;
    }
    public AddArticleDTO(){

    }
    public AddArticleDTO(String nome, String marca, String tipo, String colore, String taglia, Double prezzo, String sesso) {
        this.nome = nome;
        this.marca = marca;
        this.tipo = tipo;
        this.colore = colore;
        this.taglia = taglia;
        this.prezzo = prezzo;
        this.sesso = sesso;
    }
}
