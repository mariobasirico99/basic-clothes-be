package com.BasicClothes.restful_web_api.dto.feedbackDTOs;

public class AddFeedbackDTO {
    public Long id_utente;
    public Long id_scrittore;
    public String descrizione;
    public Double voto;
    public AddFeedbackDTO(){}
    public AddFeedbackDTO(Long id_utente, Long id_scrittore, String descrizione, Double voto) {
        this.id_utente = id_utente;
        this.id_scrittore = id_scrittore;
        this.descrizione = descrizione;
        this.voto = voto;
    }
}
