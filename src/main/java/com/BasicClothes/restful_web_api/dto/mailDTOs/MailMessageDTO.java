package com.BasicClothes.restful_web_api.dto.mailDTOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MailMessageDTO {
    public String receiver;
    public String subject;
    public String text;
}
