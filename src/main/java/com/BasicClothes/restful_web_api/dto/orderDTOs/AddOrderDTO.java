package com.BasicClothes.restful_web_api.dto.orderDTOs;

import com.BasicClothes.restful_web_api.model.Article;
import com.BasicClothes.restful_web_api.model.User;

public class AddOrderDTO {
    public Long mittente;
    public String pagamento;
    public Long destinatario;
    public Long articolo;
    public String city;
    public String indirizzo;
    public Long cap;
    public String username;
    public AddOrderDTO(){}
    public AddOrderDTO(Long mittente, String pagamento, Long destinatario, Long articolo, String city, String indirizzo, Long cap, String username) {
        this.mittente = mittente;
        this.pagamento = pagamento;
        this.destinatario = destinatario;
        this.articolo = articolo;
        this.city = city;
        this.indirizzo = indirizzo;
        this.cap = cap;
        this.username = username;
    }
}
