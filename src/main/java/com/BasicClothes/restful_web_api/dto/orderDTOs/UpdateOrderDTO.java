package com.BasicClothes.restful_web_api.dto.orderDTOs;

public class UpdateOrderDTO {
    public String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UpdateOrderDTO(){}

    public UpdateOrderDTO(String status) {
        this.status = status;
    }
}
