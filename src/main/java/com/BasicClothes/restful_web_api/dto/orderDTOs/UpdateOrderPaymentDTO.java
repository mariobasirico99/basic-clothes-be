package com.BasicClothes.restful_web_api.dto.orderDTOs;

public class UpdateOrderPaymentDTO {
    public String pagamento;


    public String getPagamento() {
        return pagamento;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }
    public UpdateOrderPaymentDTO() {}
    public UpdateOrderPaymentDTO(String pagamento) {
        this.pagamento = pagamento;
    }
}
