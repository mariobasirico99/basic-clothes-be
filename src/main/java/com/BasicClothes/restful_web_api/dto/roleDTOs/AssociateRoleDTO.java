package com.BasicClothes.restful_web_api.dto.roleDTOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AssociateRoleDTO {
    public String role;
    public Long userId;
}
