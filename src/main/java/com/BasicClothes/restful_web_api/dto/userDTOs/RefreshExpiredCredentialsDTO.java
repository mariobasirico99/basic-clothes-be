package com.BasicClothes.restful_web_api.dto.userDTOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RefreshExpiredCredentialsDTO {
    public String username;
    public String oldPassword;
    public String newPassword;
    public String retypeNewPassword;
}
