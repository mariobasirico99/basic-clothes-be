package com.BasicClothes.restful_web_api.dto.userDTOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterUserDTO {
    public String email;
    public String username;
    public String password;
    public String city;
    public String indirizzo;
    public Long cap;
    public String initialRole;
    public RegisterUserDTO(){
    }
    public RegisterUserDTO(String email, String username, String password, String city, String indirizzo, Long cap, String initialRole) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.city = city;
        this.indirizzo = indirizzo;
        this.cap = cap;
        this.initialRole = initialRole;
    }

}
