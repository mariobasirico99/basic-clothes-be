package com.BasicClothes.restful_web_api.dto.userDTOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResetForgottenPasswordDTO {
    public String username;
    public String email;
}
