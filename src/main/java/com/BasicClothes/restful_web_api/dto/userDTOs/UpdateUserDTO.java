package com.BasicClothes.restful_web_api.dto.userDTOs;

public class UpdateUserDTO {
    public String email;
    public String username;
    public String city;
    public String indirizzo;
    public Long cap;

    public UpdateUserDTO() {
    }
    public UpdateUserDTO(String email, String username, String city, String indirizzo, Long cap) {
        this.email = email;
        this.username = username;
        this.city = city;
        this.indirizzo = indirizzo;
        this.cap = cap;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public Long getCap() {
        return cap;
    }

    public void setCap(Long cap) {
        this.cap = cap;
    }
}
