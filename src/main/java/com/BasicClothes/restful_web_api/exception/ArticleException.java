package com.BasicClothes.restful_web_api.exception;

public class ArticleException extends RuntimeException{
    public ArticleException(ArticleException.ArticleExceptionCode message) {
        super(String.valueOf(message));
    }

    public enum ArticleExceptionCode {
        NAME_ALREADY_USED,
        ARTICLE_NOT_FOUND,
        ID_NOT_FOUND,
        ID_USER_NOT_FOUND,
        ARTICLE_SOLD,
        ARTICLE_NOT_SOLD,
        MITTENTE_O_DESTINATARIO_NON_ESISTENTI,
        ERROR
    }
}
