package com.BasicClothes.restful_web_api.exception;

public class CustomAuthenticationException extends RuntimeException {
    public CustomAuthenticationException(AuthenticationExceptionCode message) {
        super(String.valueOf(message));
    }

    public enum AuthenticationExceptionCode {
        JWT_TOKEN_INVALID_OR_EXPIRED,
        TOKEN_BLACKLISTED
    }
}
