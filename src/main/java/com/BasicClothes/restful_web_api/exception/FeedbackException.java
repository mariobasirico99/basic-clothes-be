package com.BasicClothes.restful_web_api.exception;

public class FeedbackException extends RuntimeException{
    public FeedbackException(FeedbackException.FeedbackExceptionCode message) {
        super(String.valueOf(message));
    }
    public enum FeedbackExceptionCode {
        ID_USER_NOT_FOUND,
        ID_NOT_FOUND,
        ERROR
    }
}
