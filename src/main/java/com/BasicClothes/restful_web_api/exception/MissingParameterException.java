package com.BasicClothes.restful_web_api.exception;


public class MissingParameterException extends RuntimeException {
    public MissingParameterException() {
        super("MISSING_ONE_OR_MORE_PARAMETERS_IN_REQUEST_BODY_OR_IN_THE_PATH");
    }

    public MissingParameterException(MissingParameterExceptionCode message, String param) {
        super(String.valueOf(message).concat(":").concat(param));
    }

    public enum MissingParameterExceptionCode {
        MISSING_PARAMETER_IN_REQUEST_BODY,
        MISSING_PARAMETER_IN_REQUEST_PATH,
        MALFORMED_BODY_PARAMETER
    }
}
