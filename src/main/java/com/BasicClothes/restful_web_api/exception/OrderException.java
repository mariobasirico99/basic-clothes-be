package com.BasicClothes.restful_web_api.exception;

public class OrderException extends RuntimeException{
    public OrderException(OrderException.OrderExceptionCode message) {
        super(String.valueOf(message));
    }

    public enum OrderExceptionCode {
        SENDER_NOT_FOUND,
        ARTICLE_NOT_FOUND,
        RECEIVER_NOT_FOUND,
        ID_USER_NOT_FOUND,
        ID_NOT_FOUND,
        ERROR
    }
}
