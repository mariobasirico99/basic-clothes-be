package com.BasicClothes.restful_web_api.exception;

public class RestResourceException extends RuntimeException {
    public RestResourceException(RestResourceExceptionCode message) {
        super(String.valueOf(message));
    }

    public enum RestResourceExceptionCode {
        RESOURCE_NOT_AVAILABLE_FOR_STILL_VALID_ACCOUNT,
        UNABLE_TO_UPDATE_PASSWORD_OF_AN_OTHER_USER
    }
}
