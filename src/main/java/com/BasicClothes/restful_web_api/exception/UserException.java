package com.BasicClothes.restful_web_api.exception;

public class UserException extends RuntimeException {
    public UserException(UserExceptionCode message) {
        super(String.valueOf(message));
    }

    public enum UserExceptionCode {
        USER_NOT_FOUND,
        USER_HAS_NO_VALID_ROLE,
        WRONG_CREDENTIALS,
        USERNAME_ALREADY_USED,
        EMAIL_ALREADY_USED,
        ACCOUNT_EXPIRED,
        CREDENTIALS_EXPIRED,
        ACCOUNT_NOT_ENABLED,
        ACCOUNT_LOCKED,
        NEW_PASSWORD_DO_NOT_MATCH,
        OLD_PASSWORD_DO_NOT_MATCH,
        EMAIL_OR_USERNAME_DO_NOT_MATCH,
        EMAIL_IS_NOT_VALID,
        ERROR
    }
}
