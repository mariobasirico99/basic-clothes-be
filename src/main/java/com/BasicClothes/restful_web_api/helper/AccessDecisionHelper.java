package com.BasicClothes.restful_web_api.helper;

import com.BasicClothes.restful_web_api.model.InvalidateToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AccessDecisionHelper {

    @Autowired
    InvalidateTokenHelper invalidateTokenHelper;

    public List<InvalidateToken> findAllInvalidateToken() {
        return invalidateTokenHelper.findAll();
    }
}
