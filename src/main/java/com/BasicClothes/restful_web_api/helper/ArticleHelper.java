package com.BasicClothes.restful_web_api.helper;

import com.BasicClothes.restful_web_api.Constant;
import com.BasicClothes.restful_web_api.dto.articleDTOs.AddArticleDTO;
import com.BasicClothes.restful_web_api.dto.mailDTOs.MailMessageDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.RegisterUserDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.UpdateUserDTO;
import com.BasicClothes.restful_web_api.exception.ArticleException;
import com.BasicClothes.restful_web_api.exception.UserException;
import com.BasicClothes.restful_web_api.model.Article;
import com.BasicClothes.restful_web_api.model.Role;
import com.BasicClothes.restful_web_api.model.User;
import com.BasicClothes.restful_web_api.repository.ArticleRepository;
import com.BasicClothes.restful_web_api.repository.RoleRepository;
import com.BasicClothes.restful_web_api.dto.roleDTOs.AssociateRoleDTO;
import com.BasicClothes.restful_web_api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@Component
public class ArticleHelper{
    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    UserRepository userRepository;

    public List<Article> findByFilter(AddArticleDTO addArticleDTO){
        return articleRepository.findByFilter(addArticleDTO.sesso,addArticleDTO.marca,
                addArticleDTO.taglia,addArticleDTO.colore,addArticleDTO.tipo,addArticleDTO.id_utente);
    };
    public List<Article> findArticlesNotMine(Long id_utente){
        if(id_utente>0 && userRepository.existsById(id_utente)){
            return articleRepository.findArticlesNotMine(id_utente);
        }
        else {
            throw new ArticleException(ArticleException.ArticleExceptionCode.ID_USER_NOT_FOUND);
        }
    };
    public List<Article> findArticlesNotMineAndSex(Long id_utente, String sesso){
        if(id_utente>0 && userRepository.existsById(id_utente)){
            return articleRepository.findArticlesNotMineAndSex(id_utente,sesso);
        }
        else {
            throw new ArticleException(ArticleException.ArticleExceptionCode.ID_USER_NOT_FOUND);
        }
    };
    public Article getById(Long id){
        if(id>0 && articleRepository.existsById(id)){
            return articleRepository.findById(id).get();
        }
        else {
            throw new ArticleException(ArticleException.ArticleExceptionCode.ID_NOT_FOUND);
        }

    };

    public Long getUserById(Long id){
        return articleRepository.findUserIdByArticle(id);
    }

    public List<Article> findAll() {
        List<Article> list= articleRepository.findAllArticle();
        for (int i=0; i<list.size(); i++){
            byte[] b= decompressBytes(list.get(i).getPicture());
            list.get(i).setPicture(b);
        }
        return list;
    }
    @Transactional
    public String delete(Long id){
        if(id>0 && articleRepository.existsById(id)){
            articleRepository.deleteArticle(id);
            return "SUCCESS";
        }
        else {
            throw new ArticleException(ArticleException.ArticleExceptionCode.ID_NOT_FOUND);
        }
    };
    public boolean isAvailable(Long id){
        if(id>0 && articleRepository.existsById(id)){
            return articleRepository.isAvailable(id);
        }
        else {
            throw new ArticleException(ArticleException.ArticleExceptionCode.ID_NOT_FOUND);
        }

    }

    public byte[] getimage(Long id) {
        if(id>0 && articleRepository.existsById(id)){
            Optional<Article> a = articleRepository.findById(id);
            if (a!= null) {
                System.out.println("ciao");
                return decompressBytes(a.get().getPicture());
            }
            throw new ArticleException(ArticleException.ArticleExceptionCode.ARTICLE_NOT_FOUND);
        }
        else {
            throw new ArticleException(ArticleException.ArticleExceptionCode.ID_NOT_FOUND);
        }

    }
    public Article save(AddArticleDTO addArticleDTO) {
        //check if the article is already used
        if (!articleRepository.existsByNome(addArticleDTO.nome.toLowerCase().trim())) {
            if(addArticleDTO.id_utente > 0 && userRepository.existsById(addArticleDTO.id_utente)){
                Optional<User> user = userRepository.findById(addArticleDTO.id_utente);
                Article art = articleRepository.saveAndFlush(Article.builder()
                        .venduto(false)
                        .marca(addArticleDTO.marca.toUpperCase().trim())
                        .tipo(addArticleDTO.tipo.toUpperCase().trim())
                        .colore(addArticleDTO.colore.toUpperCase().trim())
                        .taglia(addArticleDTO.taglia.toUpperCase().trim())
                        .sesso(addArticleDTO.sesso.toUpperCase().trim())
                        .prezzo(addArticleDTO.prezzo)
                        .user(user.get())
                        .picture(addArticleDTO.picture)
                        .nome(addArticleDTO.nome.toUpperCase().trim())
                        .build());
                return art;
            }
            else {
                throw new ArticleException(ArticleException.ArticleExceptionCode.ID_USER_NOT_FOUND);
            }

        }

        throw new ArticleException(ArticleException.ArticleExceptionCode.NAME_ALREADY_USED);
    }
    public Optional<Article> updateImage(byte[] picture, Long id) {
        if(id>0 && articleRepository.existsById(id)){
            return articleRepository.findById(id).map(target -> {
                target.setPicture(picture);
                articleRepository.saveAndFlush(target);
                return target;
            });
        }
        else {
            throw new ArticleException(ArticleException.ArticleExceptionCode.ID_NOT_FOUND);
        }
    }
    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
        }
        System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
        return outputStream.toByteArray();
    }

    // uncompress the image bytes before returning it to the angular application
    public static byte[] decompressBytes(byte[] data) {
        if(data != null){
            Inflater inflater = new Inflater();
            inflater.setInput(data);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
            byte[] buffer = new byte[1024];
            try {
                while (!inflater.finished()) {
                    int count = inflater.inflate(buffer);
                    outputStream.write(buffer, 0, count);
                }
                outputStream.close();
            } catch (IOException ioe) {
            } catch (DataFormatException e) {
            }
            return outputStream.toByteArray();
        }
        byte[] emptyArray = new byte[0];
        return emptyArray;
    }
}
