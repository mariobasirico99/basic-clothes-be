package com.BasicClothes.restful_web_api.helper;

import com.BasicClothes.restful_web_api.dto.feedbackDTOs.AddFeedbackDTO;
import com.BasicClothes.restful_web_api.exception.ArticleException;
import com.BasicClothes.restful_web_api.exception.FeedbackException;
import com.BasicClothes.restful_web_api.exception.UserException;
import com.BasicClothes.restful_web_api.model.*;
import com.BasicClothes.restful_web_api.repository.FeedbackRepository;
import com.BasicClothes.restful_web_api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Component
public class FeedbackHelper {
    @Autowired
    UserRepository userRepository;
    @Autowired
    FeedbackRepository feedbackRepository;
    public Double getRanking(Long id){
        if(id>0 && userRepository.existsById(id)){
            List<Feedback> f = feedbackRepository.findFeedbackByUser_Id(id);
            double ranking=0;
            for(int i = 0; i<f.size(); i++){
                ranking += f.get(i).getVoto();
            }
            return ranking/(f.size());
        }
        else {
            throw new FeedbackException(FeedbackException.FeedbackExceptionCode.ID_USER_NOT_FOUND);
        }

    }

    public List<Feedback> findAll() {
        return feedbackRepository.findAll();
    }

    public Feedback save(AddFeedbackDTO addFeedbackDTO) {
        //check if the user exist
        if (userRepository.existsById(addFeedbackDTO.id_utente)) {
            try {
                Optional<User> user = userRepository.findById(addFeedbackDTO.id_utente);
                Optional<User> scrittore = userRepository.findById(addFeedbackDTO.id_scrittore);
                Feedback saved = feedbackRepository.saveAndFlush(Feedback.builder()
                        .user(user.get())
                        .scrittore(scrittore.get())
                        .voto(addFeedbackDTO.voto)
                        .descrizione(addFeedbackDTO.descrizione)
                        .build());
                return saved;
            }
            catch (NoSuchElementException e) {
                throw new FeedbackException(FeedbackException.FeedbackExceptionCode.ID_USER_NOT_FOUND);
            }
        }
        throw new UserException(UserException.UserExceptionCode.USER_NOT_FOUND);
    }
    public List<Feedback> findFeedbackByUser_Id(Long id){
        if(id>0 && userRepository.existsById(id)){
            return feedbackRepository.findFeedbackByUser_Id(id);
        }
        else {
            throw new FeedbackException(FeedbackException.FeedbackExceptionCode.ID_USER_NOT_FOUND);
        }
    }
    @Transactional
    public String delete(Long id){
        if(id>0 && feedbackRepository.existsById(id)){
            feedbackRepository.deleteFeedback(id);
            return "SUCCESS";
        }
        else {
            throw new FeedbackException(FeedbackException.FeedbackExceptionCode.ID_NOT_FOUND);
        }
    };
}

