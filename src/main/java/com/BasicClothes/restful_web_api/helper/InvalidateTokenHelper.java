package com.BasicClothes.restful_web_api.helper;

import com.BasicClothes.restful_web_api.repository.InvalidateTokenRepository;
import com.BasicClothes.restful_web_api.model.InvalidateToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InvalidateTokenHelper {

    @Autowired
    InvalidateTokenRepository invalidateTokenRepository;

    public void save(String token) {
        invalidateTokenRepository.saveAndFlush(InvalidateToken.builder()
                .token(token)
                .build());
    }

    public void delete(InvalidateToken invalidateToken) {
        invalidateTokenRepository.delete(invalidateToken);
    }

    public List<InvalidateToken> findAll() {
        return invalidateTokenRepository.findAll();
    }

    public Boolean existsByToken(String token) {
        return invalidateTokenRepository.existsByToken(token);
    }

    public InvalidateToken findByToken(String token) {
        return invalidateTokenRepository.findByToken(token).orElseThrow(() -> new RuntimeException("INVALIDATE_TOKEN_NOT_FOUND"));
    }
}
