package com.BasicClothes.restful_web_api.helper;


import com.BasicClothes.restful_web_api.Constant;
import com.BasicClothes.restful_web_api.component.MailSenderComponent;
import com.BasicClothes.restful_web_api.dto.mailDTOs.MailMessageDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.AddOrderDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.UpdateOrderDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.UpdateOrderPaymentDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.UpdateUserDTO;
import com.BasicClothes.restful_web_api.exception.ArticleException;
import com.BasicClothes.restful_web_api.exception.FeedbackException;
import com.BasicClothes.restful_web_api.exception.OrderException;
import com.BasicClothes.restful_web_api.model.Article;
import com.BasicClothes.restful_web_api.model.Feedback;
import com.BasicClothes.restful_web_api.model.Order;
import com.BasicClothes.restful_web_api.model.User;
import com.BasicClothes.restful_web_api.repository.ArticleRepository;
import com.BasicClothes.restful_web_api.repository.OrderRepository;
import com.BasicClothes.restful_web_api.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
@Component
@Slf4j
public class OrderHelper {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    MailSenderComponent mailSender;

    public List<Order> findAll() {
        return orderRepository.findAllOrders();
    }

    public List<Order> getByIdDest(Long id) {
        if(id>0 && userRepository.existsById(id)){
            return orderRepository.getByIdDest(id);
        }
        else {
            throw new OrderException(OrderException.OrderExceptionCode.RECEIVER_NOT_FOUND);
        }

    }
    public List<Order> getByIdMitt(Long id) {
        if(id>0 && userRepository.existsById(id)){
            return orderRepository.getByIdMitt(id);
        }
        else {
            throw new OrderException(OrderException.OrderExceptionCode.RECEIVER_NOT_FOUND);
        }

    }

    public Order save(AddOrderDTO addOrderDTO) {
        //check if the article is already used
        if (!articleRepository.isAvailable(addOrderDTO.articolo)) {
            try {
                Optional<User> mittente = userRepository.findById(addOrderDTO.mittente);
                Optional<User> destinatario = userRepository.findById(addOrderDTO.destinatario);
                Optional<Article> articolo = articleRepository.findById(addOrderDTO.articolo);
                Article a = articolo.get();
                a.setVenduto(true);
                System.out.println(addOrderDTO);
                Order saved = orderRepository.saveAndFlush(Order.builder()
                        .mittente(mittente.get())
                        .destinatario(destinatario.get())
                        .articolo(articolo.get())
                        .status("PRESA IN CARICO")
                        .pagamento(addOrderDTO.pagamento.trim())
                        .indirizzo(addOrderDTO.indirizzo.toUpperCase().trim())
                        .city(addOrderDTO.city.toUpperCase().trim())
                        .cap(addOrderDTO.cap)
                        .username(addOrderDTO.username.trim())
                        .build());
                articleRepository.saveAndFlush(a);
                //send mail sender
                MailMessageDTO messageDTO = new MailMessageDTO();
                messageDTO.receiver = saved.getMittente().getEmail();
                messageDTO.subject = "ORDER correctly done";
                messageDTO.text = String.format(Constant.Security.ORDER_DONE_MITTENTE, saved.getArticolo().getNome(),
                        addOrderDTO.username,addOrderDTO.city,
                        addOrderDTO.indirizzo,addOrderDTO.cap);
                mailSender.sendMessage(messageDTO);
                //send mail receiver
                MailMessageDTO messageDTOdest = new MailMessageDTO();
                messageDTOdest.receiver = saved.getDestinatario().getEmail();
                messageDTOdest.subject = "ORDER correctly done";
                messageDTOdest.text = String.format(Constant.Security.ORDER_DONE_DESTINATARIO, saved.getArticolo().getNome(),
                        saved.getMittente().getEmail(), saved.getMittente().getUsername());
                mailSender.sendMessage(messageDTOdest);
                return saved;
            }
            catch (NoSuchElementException e) {
                throw new ArticleException(ArticleException.ArticleExceptionCode.MITTENTE_O_DESTINATARIO_NON_ESISTENTI);
            }
        }

        throw new ArticleException(ArticleException.ArticleExceptionCode.ARTICLE_SOLD);
    }
    public Order reso(Order ord) {
        //check if the article is already used
        if (articleRepository.isAvailable(ord.getArticolo().getId())) {
            try {
                Optional<Article> articolo = articleRepository.findById(ord.getArticolo().getId());
                Article a = articolo.get();
                a.setVenduto(false);
                ord.setStatus("RESO EFFETTUATO");
                articleRepository.saveAndFlush(a);
                //send mail sender
                MailMessageDTO messageDTO = new MailMessageDTO();
                messageDTO.receiver = ord.getDestinatario().getEmail();
                messageDTO.subject = "RESO correctly done";
                messageDTO.text = String.format(Constant.Security.RESO_DONE_DESTINATARIO, ord.getArticolo().getNome(),
                        ord.getMittente().getUsername(),ord.getMittente().getCity(),
                        ord.getMittente().getIndirizzo(),ord.getMittente().getCap());
                mailSender.sendMessage(messageDTO);
                //send mail receiver
                MailMessageDTO messageDTOmitt = new MailMessageDTO();
                messageDTOmitt.receiver = ord.getMittente().getEmail();
                messageDTOmitt.subject = "RESO correctly done";
                messageDTOmitt.text = String.format(Constant.Security.RESO_DONE_MITTENTE, ord.getArticolo().getNome(),
                        ord.getDestinatario().getEmail(), ord.getDestinatario().getUsername());
                mailSender.sendMessage(messageDTOmitt);
                return ord;
            }
            catch (NoSuchElementException e) {
                throw new ArticleException(ArticleException.ArticleExceptionCode.MITTENTE_O_DESTINATARIO_NON_ESISTENTI);
            }
        }

        throw new ArticleException(ArticleException.ArticleExceptionCode.ARTICLE_NOT_SOLD);
    }
    public Order getById(Long id){
        if(id>0 && orderRepository.existsById(id)){
            return orderRepository.findById(id).get();
        }
        else {
            throw new OrderException(OrderException.OrderExceptionCode.ID_NOT_FOUND);
        }


    };

    public Optional<Order> update(UpdateOrderDTO updateOrderDTO, Long id) {
            return orderRepository.findById(id).map(target -> {
                target.setStatus(updateOrderDTO.getStatus().toUpperCase());
                orderRepository.saveAndFlush(target);
                return target;
            });
    }
    public Optional<Order> updatePayment(UpdateOrderPaymentDTO updateOrderPaymentDTO, Long id) {
        if(id>0 && orderRepository.existsById(id)){
            return orderRepository.findById(id).map(target -> {
                target.setPagamento(updateOrderPaymentDTO.getPagamento().toUpperCase());
                orderRepository.saveAndFlush(target);
                return target;
            });
        }
        else {
            throw new OrderException(OrderException.OrderExceptionCode.ID_NOT_FOUND);
        }

    }
    @Transactional
    public String  delete(Long id){
        if(id>0 && orderRepository.existsById(id)){
            orderRepository.deleteOrder(id);
            return "SUCCESS";
        }
        else {
            throw new OrderException(OrderException.OrderExceptionCode.ID_NOT_FOUND);
        }
    };
}
