package com.BasicClothes.restful_web_api.helper;

import com.BasicClothes.restful_web_api.model.Role;
import com.BasicClothes.restful_web_api.repository.RoleRepository;
import com.BasicClothes.restful_web_api.dto.roleDTOs.AssociateRoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RoleHelper {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserHelper userHelper;

    public Role save(AssociateRoleDTO associateRoleDTO) {
        return roleRepository.saveAndFlush(Role.builder()
                .role(associateRoleDTO.role.toUpperCase())
                .user(userHelper.findById(associateRoleDTO.userId))
                .build());
    }
}
