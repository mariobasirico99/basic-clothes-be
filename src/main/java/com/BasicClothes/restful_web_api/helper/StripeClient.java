package com.BasicClothes.restful_web_api.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.stripe.Stripe;
import com.stripe.model.Charge;
import java.util.HashMap;
import java.util.Map;

@Component
public class StripeClient {



    @Autowired
    StripeClient() {
        Stripe.apiKey = "sk_test_51JRzRxEE3XQS0PxBFtP35ylvZRLEiA7KjwYUPeeiqFiXqLhaDQYP7C3XfjbwNF7K81rBj2G0KB10WPXEs8LsfkQD00X09ITHLA";
    }

    public Charge chargeCreditCard(String token, double amount) throws Exception {
        Map<String, Object> chargeParams = new HashMap<String, Object>();
        chargeParams.put("amount", (int)(amount * 100));
        chargeParams.put("currency", "EUR");
        chargeParams.put("source", token);
        Charge charge = Charge.create(chargeParams);
        return charge;
    }

}