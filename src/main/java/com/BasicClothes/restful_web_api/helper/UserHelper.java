package com.BasicClothes.restful_web_api.helper;

import com.BasicClothes.restful_web_api.Constant;
import com.BasicClothes.restful_web_api.component.MailSenderComponent;
import com.BasicClothes.restful_web_api.dto.userDTOs.*;
import com.BasicClothes.restful_web_api.exception.RestResourceException;
import com.BasicClothes.restful_web_api.exception.UserException;
import com.BasicClothes.restful_web_api.model.Role;
import com.BasicClothes.restful_web_api.model.User;
import com.BasicClothes.restful_web_api.repository.UserRepository;
import com.BasicClothes.restful_web_api.security.JwtTokenProvider;
import com.BasicClothes.restful_web_api.dto.AuthenticationResponseDTO;
import com.BasicClothes.restful_web_api.dto.TokenPassingDTO;
import com.BasicClothes.restful_web_api.dto.mailDTOs.MailMessageDTO;
import com.BasicClothes.restful_web_api.dto.roleDTOs.AssociateRoleDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class UserHelper implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    InvalidateTokenHelper invalidateTokenHelper;

    @Autowired
    RoleHelper roleHelper;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    MailSenderComponent mailSender;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        List<SimpleGrantedAuthority> roles = new ArrayList<>();
        User loadedUser = userRepository.findByUsername(s).orElseThrow(() -> new UserException(UserException.UserExceptionCode.USER_NOT_FOUND));
        userRepository.findUserRolesByUsername(loadedUser.getUsername()).forEach(role -> {
            roles.add(new SimpleGrantedAuthority(role.getRole()));
        });
        if (roles.size() > 0) {
            return org.springframework.security.core.userdetails.User
                    .withUsername(loadedUser.getUsername())
                    .password(loadedUser.getPassword())
                    .authorities(roles)
                    .accountExpired(!loadedUser.getAccountNonExpired())
                    .accountLocked(!loadedUser.getAccountNonLocked())
                    .credentialsExpired(!loadedUser.getCredentialsNonExpired())
                    .disabled(!loadedUser.getEnabled())
                    .build();
        }
        throw new UserException(UserException.UserExceptionCode.USER_HAS_NO_VALID_ROLE);
    }

    public AuthenticationResponseDTO authenticate(UserAuthenticationDTO userAuthenticationDTO) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userAuthenticationDTO.username.trim().toLowerCase(), userAuthenticationDTO.password));
            User u = userRepository.findByUsername(userAuthenticationDTO.username.trim().toLowerCase()).orElseThrow(() -> new UserException(UserException.UserExceptionCode.USER_NOT_FOUND));
            List<Role> userRoles = userRepository.findUserRolesByUsername(u.getUsername());
            TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
            return AuthenticationResponseDTO.builder()
                    .userId(u.getId())
                    .username(u.getUsername())
                    .token(tokenReturn.token)
                    .issuedAt(tokenReturn.issuedAt.toInstant())
                    .expireAt(tokenReturn.expireAt.toInstant())
                    .roles(userRoles)
                    .build();
        } catch (AccountExpiredException e) {
            throw new UserException(UserException.UserExceptionCode.ACCOUNT_EXPIRED);
        } catch (CredentialsExpiredException e) {
            throw new UserException(UserException.UserExceptionCode.CREDENTIALS_EXPIRED);
        } catch (DisabledException e) {
            throw new UserException(UserException.UserExceptionCode.ACCOUNT_NOT_ENABLED);
        } catch (LockedException e) {
            throw new UserException(UserException.UserExceptionCode.ACCOUNT_LOCKED);
        } catch (AuthenticationException e) {
            throw new UserException(UserException.UserExceptionCode.WRONG_CREDENTIALS);
        }catch (Exception e) {
            throw new UserException(UserException.UserExceptionCode.ERROR);
        }

    }

    public User save(RegisterUserDTO registerUserDTO) {
        //check if the username is already used
        if (!userRepository.existsByUsername(registerUserDTO.username)) {
            //check if the email is already used
            if (!userRepository.existsByEmail(registerUserDTO.email)) {
                //save the new user
                User saved = userRepository.saveAndFlush(User.builder()
                        .email(registerUserDTO.email.trim())
                        .username(registerUserDTO.username.trim())
                        .password(passwordEncoder.encode(registerUserDTO.password))
                        .city(registerUserDTO.city.toUpperCase().trim())
                        .indirizzo(registerUserDTO.indirizzo.toUpperCase().trim())
                        .cap(registerUserDTO.cap)
                        .enabled(true)
                        .accountNonExpired(true)
                        .credentialsNonExpired(true)
                        .accountNonLocked(true)
                        .build());
                //create a roleDTO to save the association between user and initial role
                AssociateRoleDTO associateRoleDTO = new AssociateRoleDTO();
                associateRoleDTO.userId = saved.getId();
                System.out.println(associateRoleDTO.userId);
                System.out.println(registerUserDTO.initialRole);
                associateRoleDTO.role = registerUserDTO.initialRole.toUpperCase();
                System.out.println("ROLE "+associateRoleDTO.role);
                //save the role
                roleHelper.save(associateRoleDTO);
                //send mail with registration information
                MailMessageDTO messageDTO = new MailMessageDTO();
                messageDTO.receiver = saved.getEmail();
                messageDTO.subject = "Registration correctly done";
                messageDTO.text = String.format(Constant.Security.ACCOUNT_CREATION_MESSAGE, saved.getUsername());
                mailSender.sendMessage(messageDTO);
                //return the saved user
                return saved;

            }
            throw new UserException(UserException.UserExceptionCode.EMAIL_ALREADY_USED);
        }
        throw new UserException(UserException.UserExceptionCode.USERNAME_ALREADY_USED);
    }

    public void logout(String token) {
        invalidateTokenHelper.save(token);
    }

    public User findById(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UserException(UserException.UserExceptionCode.USER_NOT_FOUND));
    }

    public List<User> findAll() {
        return userRepository.findAllUsers();
    }

    public void expireUserCredential(User user) {
        user.setCredentialsNonExpired(false);
        userRepository.saveAndFlush(user);
    }

    private User findByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new UserException(UserException.UserExceptionCode.USER_NOT_FOUND));
    }

    public User refreshExpiredPassword(RefreshExpiredCredentialsDTO refreshExpiredCredentialsDTO) {
        User u = findByUsername(refreshExpiredCredentialsDTO.username);
        try {
            UserAuthenticationDTO auth = new UserAuthenticationDTO();
            auth.username = refreshExpiredCredentialsDTO.username;
            auth.password = refreshExpiredCredentialsDTO.oldPassword;
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(refreshExpiredCredentialsDTO.username.trim().toLowerCase(), refreshExpiredCredentialsDTO.oldPassword));
        } catch (CredentialsExpiredException e) {
            if (refreshExpiredCredentialsDTO.newPassword.equals(refreshExpiredCredentialsDTO.retypeNewPassword)) {
                //set new password
                u.setPassword(passwordEncoder.encode(refreshExpiredCredentialsDTO.newPassword));
                //re-enable credentials
                u.setCredentialsNonExpired(true);
                //return saved user
                return userRepository.saveAndFlush(u);
            }
            log.error("new password do not match");
            throw new UserException(UserException.UserExceptionCode.NEW_PASSWORD_DO_NOT_MATCH);
        } catch (AuthenticationException e) {
            throw new UserException(UserException.UserExceptionCode.OLD_PASSWORD_DO_NOT_MATCH);
        }
        //the authentication gone right so it is not already the moment to refresh expired credentials
        log.error("credentials not still expired");
//        throw new UserException(CREDENTIALS_NOT_STILL_EXPIRED);
        throw new RestResourceException(RestResourceException.RestResourceExceptionCode.RESOURCE_NOT_AVAILABLE_FOR_STILL_VALID_ACCOUNT);
    }

    public User changePassword(RefreshExpiredCredentialsDTO refreshExpiredCredentialsDTO, HttpServletRequest httpServletRequest) {
        //check if username exists
        if (userRepository.existsByUsername(refreshExpiredCredentialsDTO.username)) {
            //check if the logged user is the same that want to change the password
            if (jwtTokenProvider.getUsername(httpServletRequest.getHeader("Authorization").substring(7)).equals(refreshExpiredCredentialsDTO.username)) {
                try {
                    //try to authenticate to see if the old password is correct
                    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(refreshExpiredCredentialsDTO.username.trim().toLowerCase(), refreshExpiredCredentialsDTO.oldPassword));
                } catch (AuthenticationException e) {
                    throw new UserException(UserException.UserExceptionCode.OLD_PASSWORD_DO_NOT_MATCH);
                }
                //check if the new password and the retype are the same
                if (refreshExpiredCredentialsDTO.newPassword.equals(refreshExpiredCredentialsDTO.retypeNewPassword)) {
                    User u = findByUsername(refreshExpiredCredentialsDTO.username);
                    u.setPassword(passwordEncoder.encode(refreshExpiredCredentialsDTO.newPassword));
                    return userRepository.saveAndFlush(u);
                }
                throw new UserException(UserException.UserExceptionCode.NEW_PASSWORD_DO_NOT_MATCH);
            }
            throw new RestResourceException(RestResourceException.RestResourceExceptionCode.UNABLE_TO_UPDATE_PASSWORD_OF_AN_OTHER_USER);
        }
        throw new UserException(UserException.UserExceptionCode.USER_NOT_FOUND);
    }

    public Optional<User> update(UpdateUserDTO updateUserDTO, Long id) {
        return userRepository.findById(id).map(target -> {
            target.setEmail(updateUserDTO.getEmail());
            target.setCap(updateUserDTO.getCap());
            target.setIndirizzo(updateUserDTO.getIndirizzo().toUpperCase().trim());
            target.setCity(updateUserDTO.getCity().toUpperCase().trim());
            target.setUsername(updateUserDTO.getUsername().trim());
            userRepository.saveAndFlush(target);
            return target;
        });
    }
    public User getById(Long id){
        if(id>0 && userRepository.existsById(id)){
            return userRepository.findById(id).get();
        }
        else {
            throw new UserException(UserException.UserExceptionCode.USER_NOT_FOUND);
        }
    };

    @Transactional
    public String delete(Long id){
        if(id>0 && userRepository.existsById(id)){
            userRepository.deleteUser(id);
            return "Success";
        }
        else {
            throw new UserException(UserException.UserExceptionCode.USER_NOT_FOUND);
        }

    };
}
