package com.BasicClothes.restful_web_api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.Instant;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Table(name="article")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(nullable = false)
    private String nome;

    @Column()
    private String tipo = "";

    @Column()
    private String marca = "";

    @Column(nullable = false)
    private String taglia = "";

    @Column(nullable = false, columnDefinition = "bit(1) default 0")
    private Boolean venduto;

    @Column()
    private String colore = "";

    @Column()
    private String sesso ="";

    @Column()
    private Double prezzo = 0.0;

    @Column(name = "picture", length = 10000)
    private byte[] picture;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @Getter(AccessLevel.NONE)
    @ToString.Exclude
    private User user;

    @OneToOne(mappedBy = "articolo",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Order order;


    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

}
