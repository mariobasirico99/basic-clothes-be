package com.BasicClothes.restful_web_api.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(nullable = false,name = "status")
    private String status = "PRESA IN CARICO";

    @Column(nullable = false,name = "pagamento")
    private String pagamento = "NON PAGATO";

    @Column(nullable = false,name = "username")
    private String username ="";

    @Column(name = "indirizzo")
    private String indirizzo ="";

    @Column(name = "city")
    private String city = "";

    @Column(name = "cap")
    private Long cap;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "mittente_id", referencedColumnName = "id")
    private User mittente;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "destinatario_id", referencedColumnName = "id")
    private User destinatario;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "articolo_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Article articolo;
}
