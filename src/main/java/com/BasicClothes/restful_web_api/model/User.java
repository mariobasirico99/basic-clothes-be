package com.BasicClothes.restful_web_api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name="user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false, unique = true)
    private String email;

    public User(Long id, String username, String email, String password) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String indirizzo;

    @Column(nullable = false)
    private Long cap;

    @Column(nullable = false, columnDefinition = "bit(1) default 1")
    private Boolean enabled;

    @Column(nullable = false, columnDefinition = "bit(1) default 1")
    private Boolean accountNonExpired;

    @Column(nullable = false, columnDefinition = "bit(1) default 1")
    private Boolean credentialsNonExpired;

    @Column(nullable = false, columnDefinition = "bit(1) default 1")
    private Boolean accountNonLocked;

    @OneToOne(mappedBy = "destinatario",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Order order;

    @OneToOne(mappedBy = "user",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Feedback feedback;

    @OneToOne(mappedBy = "scrittore",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Feedback feedback2;

    @OneToOne(mappedBy = "mittente",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Order order2;

    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    @ToString.Exclude
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Role> roles;

    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    @ToString.Exclude
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Article> articoli;


}
