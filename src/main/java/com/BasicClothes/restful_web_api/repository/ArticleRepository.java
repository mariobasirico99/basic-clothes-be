package com.BasicClothes.restful_web_api.repository;

import com.BasicClothes.restful_web_api.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    Optional<Article> findByTaglia(String taglia);
    Optional<Article> findById(Long id);
    @Query("select a.venduto from Article a where a.id=:id")
    boolean isAvailable(Long id);

    boolean existsByNome(String nome);

    boolean existsById(Long id);

    @Query("select a from Article a" +
            " join User u on u.id=a.user.id" +
            " where u.id=:id")
    List<Article> findArticleByUserId(Long id);

    @Query("select a.user.id from Article a" +
            " where a.id=:id")
    Long findUserIdByArticle(Long id);

    @Query("select a from Article a")
    List<Article> findAllArticle();

    @Query("select a from Article a where (a.sesso=:sesso or :sesso is null) "+
            "and (a.taglia=:taglia or :taglia is null) and (a.colore=:colore or :colore is null)"+
            "and (a.tipo=:tipo or :tipo is null) and (a.marca=:marca or :marca is null)" +
            "and (a.user.id=:id_utente or :id_utente is null)")
    List<Article> findByFilter(String sesso, String marca, String taglia, String colore, String tipo,Long id_utente);

    @Query("select a from Article a where(a.user.id!=:id_utente or :id_utente is null)" +
            "and (a.sesso=:sesso or :sesso is null)")
    List<Article> findArticlesNotMineAndSex(Long id_utente, String sesso);
    @Query("select a from Article a where(a.user.id!=:id_utente or :id_utente is null)")
    List<Article> findArticlesNotMine(Long id_utente);

    @Modifying
    @Query("delete from Article a where a.id=:id")
    void deleteArticle(Long id);
}
