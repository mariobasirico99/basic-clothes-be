package com.BasicClothes.restful_web_api.repository;


import com.BasicClothes.restful_web_api.model.Feedback;
import com.BasicClothes.restful_web_api.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FeedbackRepository extends JpaRepository<Feedback, Long> {
    @Query("select f from Feedback f")
    List<Feedback> findAll();
    @Query("select f from Feedback f where f.user.id=:id")
    List<Feedback> findFeedbackByUser_Id(Long id);

    @Modifying
    @Query("delete from Feedback f where f.id=:id")
    void deleteFeedback(Long id);

    boolean existsById(Long id);
}
