package com.BasicClothes.restful_web_api.repository;

import com.BasicClothes.restful_web_api.model.InvalidateToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface InvalidateTokenRepository extends JpaRepository<InvalidateToken, Long> {
    Boolean existsByToken(String token);

    Optional<InvalidateToken> findByToken(String token);
}
