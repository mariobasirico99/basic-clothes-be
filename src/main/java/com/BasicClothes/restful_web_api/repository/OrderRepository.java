package com.BasicClothes.restful_web_api.repository;

import com.BasicClothes.restful_web_api.model.Article;
import com.BasicClothes.restful_web_api.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query("select o from Order o")
    List<Order> findAllOrders();

    @Query("select o from Order o" +
            " where o.destinatario.id=:id")
    List<Order> getByIdDest(Long id);

    @Query("select o from Order o" +
            " where o.mittente.id=:id")
    List<Order> getByIdMitt(Long id);

    Optional<Order> findById(Long id);

    boolean existsById(Long id);

    @Modifying
    @Query("delete from Order o where o.id=:id")
    void deleteOrder(Long id);
}
