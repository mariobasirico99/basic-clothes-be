package com.BasicClothes.restful_web_api.repository;

import com.BasicClothes.restful_web_api.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {
    List<Role> findAllByUserId(Long id);
}
