package com.BasicClothes.restful_web_api.repository;

import com.BasicClothes.restful_web_api.dto.userDTOs.UpdateUserDTO;
import com.BasicClothes.restful_web_api.model.Role;
import com.BasicClothes.restful_web_api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Optional<User> findById(Long id);

    @Query(value = "SELECT * FROM role r WHERE r.user_id.id = :id_user",
            nativeQuery = true)
    List<Role> findByLastUser_Id(@Param("id_user")Long id_user);

    @Query("select r from Role r" +
            " join User u on u.id=r.user.id" +
            " where u.username=:username")
    List<Role> findUserRolesByUsername(String username);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    boolean existsById(Long id);

    @Query("select u from User u")
    List<User> findAllUsers();

    @Modifying
    @Query("delete from User u where u.id=:id")
    void deleteUser(Long id);
}
