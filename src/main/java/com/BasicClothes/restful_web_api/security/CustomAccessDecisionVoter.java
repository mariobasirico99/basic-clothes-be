package com.BasicClothes.restful_web_api.security;

import com.BasicClothes.restful_web_api.helper.AccessDecisionHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.FilterInvocation;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Component
@Slf4j
public class CustomAccessDecisionVoter implements AccessDecisionVoter<Object> {

    private final AccessDecisionHelper accessDecisionHelper;

    public CustomAccessDecisionVoter() {
        this.accessDecisionHelper = null;
    }

    public CustomAccessDecisionVoter(AccessDecisionHelper accessDecisionHelper) {
        this.accessDecisionHelper = accessDecisionHelper;
    }

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public int vote(Authentication authentication, Object o, Collection collection) {
        FilterInvocation x = (FilterInvocation) o;
        String username = authentication.getPrincipal() instanceof User ? ((User) (authentication.getPrincipal())).getUsername() : "anonymousUser";
        Collection<GrantedAuthority> role = authentication.getPrincipal() instanceof User ? ((User) (authentication.getPrincipal())).getAuthorities() : new ArrayList<>();
//        String username = authentication.getPrincipal().toString().contains("anonymousUser") ? "anonymousUser" : ((User) (authentication.getPrincipal())).getUsername();
//        Collection<GrantedAuthority> role = authentication.getPrincipal().toString().contains("anonymousUser") ? new ArrayList<>() : ((User) (authentication.getPrincipal())).getAuthorities();
        String requiredEndpoint = x.getRequestUrl();
        String method = x.getRequest().getMethod();

        log.info("Authenticated user --> ".concat(username).concat(" asking for --> ".concat(requiredEndpoint).concat(" with method --> ").concat(method).concat(" with roles --> ").concat(role.toString())));
        if (Objects.nonNull(accessDecisionHelper)) {
            //do realtime check with database info
        }

        return ACCESS_ABSTAIN;
    }

    @Override
    public boolean supports(Class aClass) {
        return true;
    }
}
