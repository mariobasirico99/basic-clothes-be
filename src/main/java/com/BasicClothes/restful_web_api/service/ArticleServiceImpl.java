package com.BasicClothes.restful_web_api.service;


import com.BasicClothes.restful_web_api.Constant;
import com.BasicClothes.restful_web_api.dto.articleDTOs.AddArticleDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.RegisterUserDTO;
import com.BasicClothes.restful_web_api.exception.MissingParameterException;
import com.BasicClothes.restful_web_api.helper.ArticleHelper;
import com.BasicClothes.restful_web_api.helper.UserHelper;
import com.BasicClothes.restful_web_api.model.Article;
import com.BasicClothes.restful_web_api.model.User;
import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
@Service
@Slf4j
public class ArticleServiceImpl{
    @Autowired
    ArticleHelper articleHelper;

    public List<Article> findAllArticle() {
        return new ArrayList<>(articleHelper.findAll());
    }

    public Article add(AddArticleDTO addArticleDTO) {
        Article a = articleHelper.save(addArticleDTO);
        return a;
    }
    public Long findUserById(Long id){
        Long iduser = articleHelper.getUserById(id);
        return iduser;
    }
    public Article getById(Long id) {
        Article a = articleHelper.getById(id);
        return a;
    }
    @Transactional
    public String delete(Long id){
        return articleHelper.delete(id);
    };
    public Optional<Article> updateImage(byte[] picture, Long id) {
        try {
            Preconditions.checkArgument(Objects.nonNull(picture));
        } catch (Exception e) {
            throw new MissingParameterException();
        }
        Optional<Article> a = articleHelper.updateImage(picture, id);
        return a;

    }
    public byte[] getimage(Long id) {
        byte[] img = articleHelper.getimage(id);
        return img;
    }
    public byte[] compressBytes(byte[] data){
        return articleHelper.compressBytes(data);
    }
    public byte[] decompressBytes(byte[] data){
        return articleHelper.decompressBytes(data);
    }
    public List<Article> findArticlesNotMine(Long id_utente){ return articleHelper.findArticlesNotMine(id_utente);}
    public List<Article> findArticlesNotMineAndSex(Long id_utente, String sesso){
        return articleHelper.findArticlesNotMineAndSex(id_utente,sesso);
    }
    public List<Article> findByFilter(AddArticleDTO addArticleDTO) {
        return articleHelper.findByFilter(addArticleDTO);
    }

}
