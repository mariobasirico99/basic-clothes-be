package com.BasicClothes.restful_web_api.service;

import com.BasicClothes.restful_web_api.dto.feedbackDTOs.AddFeedbackDTO;
import com.BasicClothes.restful_web_api.helper.FeedbackHelper;
import com.BasicClothes.restful_web_api.model.Feedback;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Slf4j
public class FeedbackServiceImpl{
    @Autowired
    FeedbackHelper feedbackHelper;

    public List<Feedback> findAll() {
        return feedbackHelper.findAll();
    }

    public List<Feedback> findFeedbackByUser_Id(Long id) {
        List<Feedback> f = feedbackHelper.findFeedbackByUser_Id(id);
        return f;
    }
    @Transactional
    public String delete(Long id){
        return feedbackHelper.delete(id);
    };
    public Feedback add(AddFeedbackDTO addFeedbackDTO) {
        Feedback f = feedbackHelper.save(addFeedbackDTO);
        return f;
    }

    public Double getRanking(Long id) {
        Double ranking = feedbackHelper.getRanking(id);
        return ranking;
    }

}
