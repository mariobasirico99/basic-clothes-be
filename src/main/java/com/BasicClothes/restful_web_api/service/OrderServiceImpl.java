package com.BasicClothes.restful_web_api.service;
import com.BasicClothes.restful_web_api.dto.articleDTOs.AddArticleDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.AddOrderDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.UpdateOrderDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.UpdateOrderPaymentDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.UpdateUserDTO;
import com.BasicClothes.restful_web_api.exception.MissingParameterException;
import com.BasicClothes.restful_web_api.helper.OrderHelper;
import com.BasicClothes.restful_web_api.model.Article;
import com.BasicClothes.restful_web_api.model.Order;
import com.BasicClothes.restful_web_api.model.User;
import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
@Service
@Slf4j
public class OrderServiceImpl{
    @Autowired
    OrderHelper orderHelper;
    public List<Order> findAll() {
        return new ArrayList<>(orderHelper.findAll());
    }

    public Order add(AddOrderDTO addOrderDTO) {
        Order order = orderHelper.save(addOrderDTO);
        return order;
    }
    public Order reso( Order ord) {
        Order order = orderHelper.reso(ord);
        return order;
    }
    public Order getById(Long id) {
        Order order = orderHelper.getById(id);
        return order;
    }
    public List<Order> getByIdDest(Long id) {
        return new ArrayList<>(orderHelper.getByIdDest(id));
    }
    public List<Order> getByIdMitt(Long id) {
        return new ArrayList<>(orderHelper.getByIdMitt(id));
    }

    @Transactional
    public String delete(Long id){
        return orderHelper.delete(id);
    };
    public Order update(UpdateOrderDTO updateOrderDTO, Long id) {
        try {
            Preconditions.checkArgument(Objects.nonNull(updateOrderDTO.status));
        } catch (Exception e) {
            throw new MissingParameterException();
        }
        Optional<Order> order = orderHelper.update(updateOrderDTO, id);
        return order.get();
    }

    public Order updatePayment(UpdateOrderPaymentDTO updateOrderPaymentDTO, Long id) {
        try {
            Preconditions.checkArgument(Objects.nonNull(updateOrderPaymentDTO.pagamento));
        } catch (Exception e) {
            throw new MissingParameterException();
        }
        Optional<Order> order = orderHelper.updatePayment(updateOrderPaymentDTO, id);
        return order.get();
    }
}
