package com.BasicClothes.restful_web_api.service;

import com.BasicClothes.restful_web_api.Constant;
import com.BasicClothes.restful_web_api.dto.userDTOs.*;
import com.BasicClothes.restful_web_api.exception.MissingParameterException;
import com.google.common.base.Preconditions;
import com.google.common.net.HttpHeaders;
import com.BasicClothes.restful_web_api.dto.AuthenticationResponseDTO;
import com.BasicClothes.restful_web_api.helper.UserHelper;
import com.BasicClothes.restful_web_api.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl {

    @Autowired
    UserHelper userHelper;

    public AuthenticationResponseDTO authenticate(UserAuthenticationDTO userAuthenticationDTO) {
        try {
            Preconditions.checkArgument(Objects.nonNull(userAuthenticationDTO.username));
            Preconditions.checkArgument(Objects.nonNull(userAuthenticationDTO.password));
        } catch (Exception e) {
            throw new MissingParameterException();
        }
        return userHelper.authenticate(userAuthenticationDTO);
    }

    public void logout(HttpServletRequest httpServletRequest) {
        String token = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION).substring(7);
        log.info(String.format("Token to be invalidated --> %s", token));
        userHelper.logout(token);
    }

    public User refreshExpiredCredentials(RefreshExpiredCredentialsDTO refreshExpiredCredentialsDTO) {
        try {
            Preconditions.checkArgument(Objects.nonNull(refreshExpiredCredentialsDTO.username));
            Preconditions.checkArgument(Objects.nonNull(refreshExpiredCredentialsDTO.oldPassword));
            Preconditions.checkArgument(Objects.nonNull(refreshExpiredCredentialsDTO.newPassword));
            Preconditions.checkArgument(Objects.nonNull(refreshExpiredCredentialsDTO.retypeNewPassword));
        } catch (Exception e) {
            throw new MissingParameterException();
        }
        User u = userHelper.refreshExpiredPassword(refreshExpiredCredentialsDTO);
        u.setPassword(Constant.Security.PROTECTED);
        return u;
    }

    public List<User> findAll() {
        return userHelper.findAll().stream().peek(o -> o.setPassword(Constant.Security.PROTECTED)).collect(Collectors.toList());
    }

    public User changePassword(RefreshExpiredCredentialsDTO refreshExpiredCredentialsDTO, HttpServletRequest httpServletRequest) {
        try {
            Preconditions.checkArgument(Objects.nonNull(refreshExpiredCredentialsDTO.username));
            Preconditions.checkArgument(Objects.nonNull(refreshExpiredCredentialsDTO.oldPassword));
            Preconditions.checkArgument(Objects.nonNull(refreshExpiredCredentialsDTO.newPassword));
            Preconditions.checkArgument(Objects.nonNull(refreshExpiredCredentialsDTO.retypeNewPassword));
        } catch (Exception e) {
            throw new MissingParameterException();
        }
        User u = userHelper.changePassword(refreshExpiredCredentialsDTO, httpServletRequest);
        u.setPassword(Constant.Security.PROTECTED);
        return u;
    }

    public User register(RegisterUserDTO registerUserDTO) {
        try {
            Preconditions.checkArgument(Objects.nonNull(registerUserDTO.username));
            Preconditions.checkArgument(Objects.nonNull(registerUserDTO.email));
            Preconditions.checkArgument(Objects.nonNull(registerUserDTO.password));
            Preconditions.checkArgument(Objects.nonNull(registerUserDTO.city));
            Preconditions.checkArgument(Objects.nonNull(registerUserDTO.indirizzo));
            Preconditions.checkArgument(Objects.nonNull(registerUserDTO.cap));
            Preconditions.checkArgument(Objects.nonNull(registerUserDTO.initialRole));
        } catch (Exception e) {
            throw new MissingParameterException();
        }
        User u = userHelper.save(registerUserDTO);
        u.setPassword(Constant.Security.PROTECTED);
        return u;
    }
    @Transactional
    public String delete(Long id){
        return userHelper.delete(id);
    };

    public User update(UpdateUserDTO updateUserDTO, Long id) {
        try {
            Preconditions.checkArgument(Objects.nonNull(updateUserDTO.username));
            Preconditions.checkArgument(Objects.nonNull(updateUserDTO.email));
            Preconditions.checkArgument(Objects.nonNull(updateUserDTO.city));
            Preconditions.checkArgument(Objects.nonNull(updateUserDTO.indirizzo));
            Preconditions.checkArgument(Objects.nonNull(updateUserDTO.cap));
        } catch (Exception e) {
            throw new MissingParameterException();
        }
        Optional<User> u = userHelper.update(updateUserDTO, id);
        return u.get();
    }
    public User getById(Long id) {
        User user = userHelper.getById(id);
        return user;
    }
}
