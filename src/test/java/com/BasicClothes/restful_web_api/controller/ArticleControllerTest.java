package com.BasicClothes.restful_web_api.controller;
import com.BasicClothes.restful_web_api.ContextConfigurationClass;
import com.BasicClothes.restful_web_api.dto.TokenPassingDTO;
import com.BasicClothes.restful_web_api.dto.articleDTOs.AddArticleDTO;
import com.BasicClothes.restful_web_api.model.Article;
import com.BasicClothes.restful_web_api.model.Role;
import com.BasicClothes.restful_web_api.model.User;
import com.BasicClothes.restful_web_api.security.JwtTokenProvider;
import com.BasicClothes.restful_web_api.service.ArticleServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ContextConfiguration(classes = ContextConfigurationClass.class)
public class ArticleControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    private MockMvc mockMvc;
    private User u = createUser();
    private List<Role> userRoles = new ArrayList<>();
    private Role r = new Role(Long.parseLong("1"),"ROLE_ADMIN");

    @MockBean
    ArticleServiceImpl service;


    @Before
    public void setUp() throws Exception {

        //Inialization of mocks
        MockitoAnnotations.initMocks(this);
        userRoles.add(r);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        //define the behavior of service.authenticate method


    }
    @Test //article
    public void add() throws Exception {
        when(service.add(any())).thenReturn(createArticle());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.post("/article/add")
                .header("Authentication","Bearer " + tokenReturn)
                .content(asJsonString(new AddArticleDTO("maglietta","test","test","test","M",1.0,"test")))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(createArticle().getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.tipo").value(createArticle().getTipo()));

    }
    @Test //article
    public void getById() throws Exception {
        when(service.getById(anyLong())).thenReturn(createArticle());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        //GET FROM HBASE
        MvcResult result = mockMvc.perform(get("/article/getById")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id", "1")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        Article articleResponse = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Article.class);
        areEquals(articleResponse, createArticle());
    }
    @Test //article
    public void notMine() throws Exception {
        Article first = Article.builder().id(Long.parseLong("2")).nome("test1").colore("test1").tipo("test1").build();
        Article second = Article.builder().id(Long.parseLong("3")).nome("test2").colore("test2").tipo("test2").build();
        when(service.findArticlesNotMine(anyLong())).thenReturn(Arrays.asList(first, second));
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        //GET FROM HBASE
        mockMvc.perform(get("/article/notMine")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id", "1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", is("test1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].nome", is("test2")));
    }
    @Test
    public void getAll() throws Exception{
        Article first = Article.builder().id(Long.parseLong("1")).nome("test1").colore("test1").tipo("test1").build();
        Article second = Article.builder().id(Long.parseLong("2")).nome("test2").colore("test2").tipo("test2").build();
        when(service.findAllArticle()).thenReturn(Arrays.asList(first, second));
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.get("/article/getall")
                .header("Authentication","Bearer " + tokenReturn)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", is("test1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].nome", is("test2")));
    }
    @Test
    public void delete() throws Exception{
        when(service.delete(anyLong())).thenReturn("SUCCESS");
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.delete("/article/delete")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id","1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    public Article createArticle() {
        return Article.builder()
                .id(Long.parseLong("1"))
                .nome("maglietta")
                .user(createUser())
                .colore("test")
                .taglia("M")
                .marca("test")
                .prezzo(1.0)
                .sesso("test")
                .tipo("test")
                .build();
    }
    public static String asJsonString(final Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }
    public User createUser() {
        return User.builder()
                .id(Long.parseLong("1"))
                .username("admin")
                .password("admin")
                .email("admin@mail.it")
                .build();
    }
    public void areEquals(Article art1, Article art2) {

        assertEquals(art1.getId(), art2.getId());
        assertEquals(art1.getNome(), art2.getNome());

    }
}
