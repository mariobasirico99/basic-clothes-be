package com.BasicClothes.restful_web_api.controller;
import com.BasicClothes.restful_web_api.ContextConfigurationClass;
import com.BasicClothes.restful_web_api.dto.TokenPassingDTO;
import com.BasicClothes.restful_web_api.dto.feedbackDTOs.AddFeedbackDTO;
import com.BasicClothes.restful_web_api.model.Feedback;
import com.BasicClothes.restful_web_api.model.Role;
import com.BasicClothes.restful_web_api.model.User;
import com.BasicClothes.restful_web_api.security.JwtTokenProvider;
import com.BasicClothes.restful_web_api.service.FeedbackServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ContextConfiguration(classes = ContextConfigurationClass.class)
public class FeedbackControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    private MockMvc mockMvc;
    private User u = createUser();
    private List<Role> userRoles = new ArrayList<>();
    private Role r = new Role(Long.parseLong("1"),"ROLE_ADMIN");

    @MockBean
    FeedbackServiceImpl service;


    @Before
    public void setUp() throws Exception {
        userRoles.add(r);
        //Inialization of mocks
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    @Test //user
    public void getRankingByUser() throws Exception {
        Double voto = 5.0;
        when(service.getRanking(anyLong())).thenReturn(voto);
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        //GET FROM HBASE
        MvcResult result = mockMvc.perform(get("/feedback/getRankingByUser")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id", "1")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        double ranking = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Double.class);
        assertEquals(java.util.Optional.of(ranking).get(), voto);
    }
    @Test
    public void getAll() throws Exception{
        Feedback first = Feedback.builder().id(Long.parseLong("1")).descrizione("desc1").voto(0.0).build();
        Feedback second = Feedback.builder().id(Long.parseLong("2")).descrizione("desc2").voto(0.0).build();
        when(service.findAll()).thenReturn(Arrays.asList(first, second));
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.get("/feedback/getall")
                .header("Authentication","Bearer " + tokenReturn)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].descrizione", is("desc1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].descrizione", is("desc2")));
    }

    @Test
    public void delete() throws Exception{
        when(service.delete(anyLong())).thenReturn("SUCCESS");
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.delete("/feedback/delete")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id","1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    @Test
    public void addFeedback() throws Exception{
        when(service.add(any())).thenReturn(createFeedback());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.post("/feedback/add")
                .header("Authentication","Bearer " + tokenReturn)
                .content(asJsonString(new AddFeedbackDTO(Long.parseLong("12112"),Long.parseLong("12112"),"test",0.0)))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.descrizione").value("test"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.voto").value(0.0));
    }

    public static String asJsonString(final Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }
    public User createUser() {
        return User.builder()
                .id(Long.parseLong("1"))
                .username("admin")
                .password("admin")
                .email("admin@mail.it")
                .build();
    }
    public Feedback createFeedback() {
        return Feedback.builder()
                .id(Long.parseLong("1"))
                .descrizione("test")
                .voto(0.0)
                .user(new User())
                .scrittore(new User())
                .build();
    }
}
