package com.BasicClothes.restful_web_api.controller;
import com.BasicClothes.restful_web_api.ContextConfigurationClass;
import com.BasicClothes.restful_web_api.dto.TokenPassingDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.AddOrderDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.UpdateOrderDTO;
import com.BasicClothes.restful_web_api.dto.orderDTOs.UpdateOrderPaymentDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.RegisterUserDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.UpdateUserDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.UserAuthenticationDTO;
import com.BasicClothes.restful_web_api.model.Article;
import com.BasicClothes.restful_web_api.model.Order;
import com.BasicClothes.restful_web_api.model.Role;
import com.BasicClothes.restful_web_api.model.User;
import com.BasicClothes.restful_web_api.security.JwtTokenProvider;
import com.BasicClothes.restful_web_api.service.OrderServiceImpl;
import com.BasicClothes.restful_web_api.service.UserServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ContextConfiguration(classes = ContextConfigurationClass.class)
public class OrderControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    private MockMvc mockMvc;
    private User u = createUser();
    private List<Role> userRoles = new ArrayList<>();
    private Role r = new Role(Long.parseLong("1"),"ROLE_ADMIN");

    @MockBean
    OrderServiceImpl service;


    @Before
    public void setUp() throws Exception {
        userRoles.add(r);
        //Inialization of mocks
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();


    }
    @Test //user
    public void getById() throws Exception {

        when(service.getById(anyLong())).thenReturn(createOrder());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        //GET FROM HBASE
        MvcResult result = mockMvc.perform(get("/order/getById")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id", "1")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        Order orderResponse = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Order.class);
        areEquals(orderResponse, createOrder());

    }
    @Test //user
    public void getByIdDest() throws Exception {

        Order first = Order.builder()
                .id(Long.parseLong("1"))
                .pagamento("PAGATO")
                .status("CONSEGNATO")
                .username("test")
                .indirizzo("test")
                .articolo(new Article())
                .destinatario(new User())
                .mittente(new User())
                .cap(Long.parseLong("12322"))
                .city("test")
                .build();
        Order second = Order.builder()
                .id(Long.parseLong("2"))
                .pagamento("PAGATO")
                .status("CONSEGNATO")
                .username("test2")
                .indirizzo("test2")
                .articolo(new Article())
                .destinatario(new User())
                .mittente(new User())
                .cap(Long.parseLong("12322"))
                .city("test2")
                .build();
        when(service.getByIdDest(anyLong())).thenReturn(Arrays.asList(first, second));
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.get("/order/getByIdDest")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id", "1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect( MockMvcResultMatchers.jsonPath("$[0].id").value(first.getId()))
                .andExpect( MockMvcResultMatchers.jsonPath("$[1].id").value(second.getId()));

    }

    @Test
    public void Reso() throws Exception{
        when(service.reso(any())).thenReturn(createOrder());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.post("/order/reso")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id", "1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(createOrder().getUsername()));
    }

    @Test
    public void AddOrder() throws Exception{
        when(service.add(any())).thenReturn(createOrder());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.post("/order/add")
                .header("Authentication","Bearer " + tokenReturn)
                .content(asJsonString(new AddOrderDTO(Long.parseLong("12112"),"PAGATO",Long.parseLong("12112"),Long.parseLong("12112"),"test","test",Long.parseLong("12112"),"test")))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(createOrder().getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.indirizzo").value(createOrder().getIndirizzo()));
    }
    @Test
    public void getAll() throws Exception{
        Order first = Order.builder()
                .id(Long.parseLong("1"))
                .pagamento("PAGATO")
                .status("CONSEGNATO")
                .username("test")
                .indirizzo("test")
                .cap(Long.parseLong("12322"))
                .city("test")
                .build();
        Order second = Order.builder()
                .id(Long.parseLong("2"))
                .pagamento("PAGATO")
                .status("CONSEGNATO")
                .username("test2")
                .indirizzo("test2")
                .cap(Long.parseLong("12322"))
                .city("test2")
                .build();
        when(service.findAll()).thenReturn(Arrays.asList(first, second));
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.get("/order/getall")
                .header("Authentication","Bearer " + tokenReturn)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect( MockMvcResultMatchers.jsonPath("$[0].id").value(first.getId()))
                .andExpect( MockMvcResultMatchers.jsonPath("$[1].id").value(second.getId()));
    }

    @Test
    public void update() throws Exception{
        when(service.update(any(),anyLong())).thenReturn(createOrder());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        String status = "CONSEGNATO";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.patch("/order/update?id=1")
                .header("Authentication","Bearer " + tokenReturn)
                .content(asJsonString("{'status':'CONSEGNATO'}"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        Order orderResponse = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Order.class);
        areEquals(orderResponse, createOrder());
    }

    @Test
    public void updatePayment() throws Exception{
        when(service.updatePayment(any(),any())).thenReturn(createOrder());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        MvcResult result = mockMvc.perform(patch("/order/updatePayment?id=1")
                .header("Authentication","Bearer " + tokenReturn)
                .content(asJsonString("{'pagamento':'PAGATO'}"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        Order orderResponse = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Order.class);
        areEquals(orderResponse, createOrder());
    }

    @Test
    public void delete() throws Exception{
        when(service.delete(anyLong())).thenReturn("SUCCESS");
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.delete("/order/delete")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id","1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) throws JsonProcessingException {
        new ObjectMapper().writeValueAsString(obj);
        return new ObjectMapper().writeValueAsString(obj);
    }
    public Order createOrder() {
        return Order.builder()
                .id(1L)
                .pagamento("PAGATO")
                .status("CONSEGNATO")
                .username("test")
                .indirizzo("test")
                .cap(Long.parseLong("12322"))
                .city("test")
                .build();
    }
    public User createUser() {
        return User.builder()
                .id(Long.parseLong("1"))
                .username("admin")
                .password("admin")
                .email("admin@mail.it")
                .build();
    }
    public void areEquals(Order order1, Order order2) {
        assertEquals(order2.getUsername(), order1.getUsername());

    }
}
