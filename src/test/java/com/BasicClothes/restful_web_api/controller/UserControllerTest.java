package com.BasicClothes.restful_web_api.controller;

import com.BasicClothes.restful_web_api.ContextConfigurationClass;
import com.BasicClothes.restful_web_api.dto.TokenPassingDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.RegisterUserDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.UpdateUserDTO;
import com.BasicClothes.restful_web_api.dto.userDTOs.UserAuthenticationDTO;
import com.BasicClothes.restful_web_api.model.Role;
import com.BasicClothes.restful_web_api.model.User;
import com.BasicClothes.restful_web_api.security.JwtTokenProvider;
import com.BasicClothes.restful_web_api.service.UserServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ContextConfiguration(classes = ContextConfigurationClass.class)
public class UserControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    private MockMvc mockMvc;
    private User u = createUser();
    private List<Role> userRoles = new ArrayList<>();
    private Role r = new Role(Long.parseLong("1"),"ROLE_ADMIN");

    @MockBean
    UserServiceImpl service;


    @Before
    public void setUp() throws Exception {
        userRoles.add(r);
        //Inialization of mocks
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();


    }
    @Test //user
    public void getById() throws Exception {

        when(service.getById(anyLong())).thenReturn(createUser());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        //GET FROM HBASE
        MvcResult result = mockMvc.perform(get("/user/getById")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id", "1")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        User userResponse = new ObjectMapper().readValue(result.getResponse().getContentAsString(), User.class);
        areEquals(userResponse, createUser());

    }

    @Test
    public void SignUpUser() throws Exception{
        when(service.register(any())).thenReturn(createUserReg());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
                .header("Authentication","Bearer " + tokenReturn)
                .content(asJsonString(new RegisterUserDTO("test@mail.it","test","test","test","test",Long.parseLong("12112"),userRoles.get(0).getRole())))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(createUserReg().getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(createUserReg().getEmail()));
    }
    @Test
    public void getAll() throws Exception{
        User first = User.builder().id(Long.parseLong("2")).username("user1").password("user1").email("user1@mail.it")
                .build();
        User second = User.builder()
                .id(Long.parseLong("3"))
                .username("user2")
                .password("user2")
                .email("user2@mail.it")
                .build();
        when(service.findAll()).thenReturn(Arrays.asList(first, second));
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.get("/user/getall")
                .header("Authentication","Bearer " + tokenReturn)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].username", is("user1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].username", is("user2")));
    }

    @Test
    public void update() throws Exception{
        when(service.update(any(),anyLong())).thenReturn(createUserReg());
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.patch("/user/update")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id","1")
                .content(asJsonString(new UpdateUserDTO("test@mail.it","test","test","test",Long.parseLong("12112"))))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(createUserReg().getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(createUserReg().getEmail()));
    }

    @Test
    public void delete() throws Exception{
        when(service.delete(anyLong())).thenReturn("Success");
        TokenPassingDTO tokenReturn = jwtTokenProvider.createToken(u.getUsername(), userRoles);
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/delete")
                .header("Authentication","Bearer " + tokenReturn)
                .param("id","1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }
    public User createUser() {
        return User.builder()
                .id(Long.parseLong("1"))
                .username("admin")
                .password("admin")
                .email("admin@mail.it")
                .build();
    }

    public User createUserReg() {
        return User.builder()
                .id(Long.parseLong("1"))
                .username("test")
                .password("test")
                .email("test@mail.it")
                .city("test")
                .indirizzo("test")
                .cap(Long.parseLong("12"))
                .build();
    }

    public void areEquals(User user1, User user2) {
        assertEquals(user1.getId(), user2.getId());
        assertEquals(user1.getUsername(), user2.getUsername());

    }

    public void areEqualsReg(User user1,User user2) {
        assertEquals(user1.getEmail(), user2.getEmail());
        assertEquals(user1.getUsername(), user2.getUsername());

    }


}




